require 'tty-prompt'
require 'tty-table'
require 'tty-editor'
require 'pastel'
require 'http'
require 'pry'

Dir['./lib/*.rb'].each { |f| require f }
Dir['./lib/operations/*.rb'].each { |f| require f }

operations = JSON.parse(HTTP.get('http://localhost:5689/api/operations').body)
operations += Operation.all.map { |op| [op.to_s.to_chain, op.signature, op.authenticator]}

prompt = TTY::Prompt.new
pastel = Pastel.new

class CLI
  class << self
    def set(k,v)
      @@cfg ||= {}
      @@cfg[k] = v
    end

    def get(k)
      @@cfg[k]
    end

    def all
      @@cfg
    end
  end
end

CLI.set('running', true)
CLI.set('location_x', -86.941119)
CLI.set('location_y', 40.470438)

while CLI.get('running')
  us = CLI.get('user_state')

  if CLI.get('token')
    if us
      print '  '
      if us['notifications'].count == 0
        print pastel.white.on_black(' %03d ! ' % us['notifications'].count)
      else
        print pastel.bold.white.on_blue(' %03d ! ' % us['notifications'].count)
      end

      print '  '
      print pastel.bold.white.on_green(' [%+03.3f, %+03.3f] ' % us['location'])

      print '  '
      print pastel.bold.black.on_yellow(' %08d ¤ ' % us['credits'])

      if us['tags'].include?('wizard')
        print '  '
        print pastel.bold.white.on_magenta(' WIZ ')
      end

      if us['tags'].include?('arbiter')
        print '  '
        print pastel.bold.white.on_red(' ARB ')
      end

      if us['tags'].include?('architect')
        print '  '
        print pastel.bold.white.on_cyan(' ARC ')
      end

      puts
    else
    end
  else

  end

  opPool = operations.select do |op|
    name, sig, auth = op
    if CLI.get('token')
      if auth.is_a? String
        rule = auth.dup
        tokens = auth.scan /\w+/
        rule.gsub!('local', 'true')
        tokens.each do |token|
          rule.gsub!(token, us['tags'].include?(token).to_s)
        end
        !!eval(rule)
      else
        auth
      end
    else
      !auth
    end
  end

  opName, signature, authenticator = prompt.select('Choose Operation', opPool.map { |o| {name: o[0], value: o} }, cycle: true, filter: true)

  data = {}

  signature.each do |se|
    data[se.to_sym] = case se 
    when 'secret', 'password'
      prompt.mask(se, mask: '*')
    when 'recipe_id'
      resp = JSON.parse(HTTP.post("http://localhost:5689/api/operate/list-recipes", body: {data: {}, token: CLI.get('token')}.to_json ).body)
      opts = resp['result']['recipes'].map do |e|
        { name: "#{(e['resources'] + e['items']).join ','} => #{e['products'].join ','}", value: e['_id'] }
      end
      prompt.select('Choose Recipe', opts, cycle: true, filter: true)
    when 'loot_table_id'
      resp = JSON.parse(HTTP.post("http://localhost:5689/api/operate/list-loot-tables", body: {data: {}, token: CLI.get('token')}.to_json ).body)
      puts resp.to_json
      opts = resp['result']['loot_tables'].map do |e|
        { name: e['name'] || '-', value: e['_id'] }
      end
      prompt.select('Choose Loot Table', opts, cycle: true, filter: true)
    when 'item_id'
      opts = CLI.get('user_state')['items'].map { |i| {name: i['name'], value: i['id']['$oid'] } }
      prompt.select('Choose Item', opts, cycle: true, filter: true)
    when 'script', 'message'
      TTY::Editor.open('.editorTemp', content: "-- Write your script here\n\n")
      res = File.read('.editorTemp')
      File.delete('.editorTemp')
      res
    when 'loot_table'
      resp = JSON.parse(HTTP.post("http://localhost:5689/api/operate/list-resources", body: {data: {}, token: CLI.get('token')}.to_json ).body)
      starter = resp['result']['resources'].map {|r| "#{r['name']}:0"}.join("\n")
      TTY::Editor.open('.editorTemp', content: starter)
      res = File.read('.editorTemp')
      File.delete('.editorTemp')
      res.split("\n").map { |l| i = l.split(':'); { i[0] => i[1].to_i } }.reduce({}) { |a,v| a.merge(v) }
    when 'property_id'
      opts = CLI.get('user_state')['properties'].map { |i| {name: "#{i['name']} | #{i['location'].join(', ')}" , value: i['_id']['$oid'] } }
      prompt.select('Choose Item', opts, cycle: true, filter: true)
    else
      prompt.ask(se)
    end
  end

  if authenticator == 'local'
    op = Operation.all.select { |o| o.to_s.to_chain == opName }.first
    op.new.run(CLI.get('user_state'), data)
  else
    begin
      req_body = {
        data: data
      }

      if CLI.get('token')
        req_body[:token] = CLI.get('token')
        req_body[:location_x] = CLI.get('location_x')
        req_body[:location_y] = CLI.get('location_y')
      end

      puts req_body.to_json

      response = JSON.parse(HTTP.post("http://localhost:5689/api/operate/#{opName}", body: req_body.to_json ).body)
      
      if response['result']

        if response['result']['auth_token']
          CLI.set('token', response['result']['auth_token']['token'])
        end

        if response['user_state']
          CLI.set('user_state', response.delete('user_state'))
        end
      end
      puts JSON.pretty_generate(response)

    rescue Exception => ex
      puts ex
      puts ex.backtrace
    end
    # binding.pry
  end
end