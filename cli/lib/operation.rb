class Operation
  def self.all
    @@operations = ObjectSpace.each_object(Class).select { |c| c < self }.sort_by{ |c| c.to_s }
  end

  def self.signature
    self.new.signature || []
  end

  def self.authenticator
    self.new.authenticator
  end

  def authenticator
    'local'
  end

  def run(token, data)
    raise "Operations must define a run method"
  end
end