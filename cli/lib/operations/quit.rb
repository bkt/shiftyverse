class Quit < Operation
  def signature
    []
  end

  def run(token, data)
    CLI.set('running', false)
  end
end