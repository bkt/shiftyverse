class Move < Operation
  def signature
    [
      :location_x,
      :location_y
    ]
  end

  def run(user, data)
    CLI.set('location_x', data[:location_x])
    CLI.set('location_y', data[:location_y])
  end
end