class GetNotifications < Operation
  def signature
    []
  end

  def run(user, data)
    table = TTY::Table.new rows: user['notifications'].map { |n| [n['created_at'], n['source'], n['message'], n['_id']['$oid']] }
    puts table.render(:unicode)
  end
end