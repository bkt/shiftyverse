class PryShell < Operation
  def signature
    []
  end

  def run(token, data)
    binding.pry
  end
end