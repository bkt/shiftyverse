require 'mongoid'
require 'sinatra'
require 'sinatra/reloader'
require 'faye'
require 'pry'
require 'rbnacl'
require 'jwt'
require 'mongoid/geospatial'

Mongoid.load!(File.join(File.dirname(__FILE__), 'mongoid.yml'))

Dir['./lib/*.rb'].each { |f| require f }
Dir['./lib/models/*.rb'].each { |f| require f }
Dir['./lib/operations/**/*.rb'].each { |f| require f }

class ShiftyverseServer < Sinatra::Base
  configure :development do
    register Sinatra::Reloader
    after_reload do
      puts " -> Reloaded"
    end
  end

  get '/' do
    
  end

  get '/api/operations/?' do
    Operation.all.sort_by { |op| op.to_s.to_chain }.map { |o| [o.to_s.to_chain, o.signature, o.authenticator] }.to_json
  end

  post '/api/operate/:operation' do
    op = Operation.all.select{ |o| o.to_s.to_chain == params[:operation]}.first
    404 unless op
    payload = JSON.parse(request.body.read).symbolize_keys
    user = nil
    if payload[:token]
      begin
        user = User.find(AuthToken.parse(payload[:token])['id'])
        if payload[:location_x] && payload[:location_y]
          user.location = {lng: payload[:location_x], lat: payload[:location_y]}
        end
        user.last_seen = Time.now
        user.save
      rescue
        return {error: 'AUTHFAIL'}
      end
    end
    
    begin
      if op.can_run_as?(user)
        response = {
          result: op.new.run(user, payload[:data].symbolize_keys)
        }
        user&.reload
        if user
          response[:user_state] = user.to_state
        elsif response[:result] && response[:result].is_a?(Hash)
          response[:user_state] = response[:result]&.fetch(:user_state, nil)
        end
        response.to_json
      else
        { result: { error: 'NOTAUTH' } }.to_json
      end
    rescue Exception => ex
      {error: "INTERR", msg: ex.message, bt: ex.backtrace}.to_json
    end
  end
end