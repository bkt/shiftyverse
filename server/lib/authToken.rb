class AuthToken
  SECRET = 'thisIsABigSecret54321!'
  ALGORITHM = 'HS256'

  attr_accessor :data

  def initialize(data={})
    @data = data
  end

  def self.parse(token)
    data = JWT.decode(token, SECRET, true, { algorithm: ALGORITHM })[0]
    new(data)
  end

  def [](key)
    @data[key]
  end

  def to_token
    JWT.encode @data, SECRET, ALGORITHM
  end
end