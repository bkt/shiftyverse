class Operation
  def self.all
    @@operations = ObjectSpace.each_object(Class).select { |c| c < self }
  end

  def self.signature
    self.new.signature
  end

  def self.authenticator
    self.new.authenticator
  end

  def self.can_run_as?(user)
    return true unless authenticator
    if authenticator.is_a? String
      rule = authenticator.dup
      tokens = authenticator.scan /\w+/
      tokens.each do |token|
        rule.gsub!(token, user.tags.include?(token).to_s)
      end

      !!eval(rule)
    else
      true
    end
  end

  def signature
    raise "Operations must define a signature"
  end

  def authenticator
    raise "Operations must define an authenticator"
  end

  def run(user, data)
    raise "Operations must define a run method"
  end
end