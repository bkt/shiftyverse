module SecurePassword
  def hashPassword(password)
    salt = ::RbNaCl::Random.random_bytes(::RbNaCl::PasswordHash::SCrypt::SALTBYTES)
    opslimit = 2**20
    memlimit = 2**24
    digest_size = 64
    hash = ::RbNaCl::PasswordHash.scrypt(
      password, 
      salt,
      opslimit,
      memlimit,
      digest_size
    )
    [::RbNaCl::Util.bin2hex(salt), ::RbNaCl::Util.bin2hex(hash)]
  end

  def checkPassword(password, hash, salt)
    salt = ::RbNaCl::Util.hex2bin(salt)
    opslimit = 2**20
    memlimit = 2**24
    digest_size = 64
    check = ::RbNaCl::PasswordHash.scrypt(
      password, 
      salt,
      opslimit,
      memlimit,
      digest_size
    )
    ::RbNaCl::Util.bin2hex(check) == hash
  end
end