require 'rufus-lua'

class ScriptEngine
  def initialize(script, executor, context)
    @script = script
    @executor = executor
    @state = Rufus::Lua::State.new
    @result = nil
  end

  def run(args = {})
    @result = @state.eval(@script)
  end

  def result
    @result
  end
end