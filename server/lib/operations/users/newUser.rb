class NewUser < Operation
  include SecurePassword
  def signature
    [
      'username',
      'password'
    ]
  end

  def authenticator
    nil
  end

  def run(user, data)
    salt, hash = hashPassword(data[:password])
    @user = User.create(
      username: data[:username],
      password_salt: salt,
      password_hash: hash
    )

    {user: @user} 
  end
end