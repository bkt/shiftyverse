class GetUser < Operation
  def signature
    [
      'username'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    if u
      if user.tags.include?('wizard') || user
        u.to_state
      else
        u.public_state
      end
    else
      {error: 'NOEXIST'}
    end
  end
end