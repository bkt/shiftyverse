class ListUsers < Operation
  def signature
    [
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    { users: User.all.map(&:public_state) }
  end
end