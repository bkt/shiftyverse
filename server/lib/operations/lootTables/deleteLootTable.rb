class DeleteLootTable < Operation
  def signature
    [
      'loot_table_id'
    ]
  end

  def authenticator
    'wizard'
  end

  def run(user, data)
    LootTable.find(data[:loot_table_id]).delete
    'OK'
  end
end