class CreateLootTable < Operation
  def signature
    [
      'name',
      'loot_table'
    ]
  end

  def authenticator
    'wizard'
  end

  def run(user,data)
    if lt = LootTable.create(name: data[:name], table: data[:loot_table])
      lt
    else
      { error: "CRETFAIL" }
    end
  end
end