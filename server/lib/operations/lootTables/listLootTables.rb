class ListLootTables < Operation
  def signature
    []
  end

  def authenticator
    true
  end

  def run(user, data)
    { loot_tables: LootTable.all }
  end
end