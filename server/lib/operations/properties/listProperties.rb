class ListProperties < Operation
  def signature
    []
  end

  def authenticator
    true
  end

  def run(user, data)
    { properties: Property.all }
  end
end