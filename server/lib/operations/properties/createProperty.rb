class CreateProperty < Operation
  def signature
    [
      'name',
      'location_x',
      'location_y'
    ]
  end

  def authenticator
    'wizard || arbiter'
  end

  def run(user, data)
    p = Property.new(ownable: user)
    p.location = {lng: data[:location_x], lat: data[:location_y]}
    p.save
    p
  end
end