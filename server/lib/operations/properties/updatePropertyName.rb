class UpdatePropertyName < Operation
  def signature
    [
      'property_id',
      'name'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    p = Property.find(data[:property_id])
    if p
      if p.ownable == user
        p.name = data[:name]
        p.save
        p
      else
        { error: 'NOTAUTH' }
      end
    else
      { error: 'NOEXIST' }
    end
  end
end