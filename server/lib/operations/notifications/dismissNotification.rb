class DismissNotification < Operation
  def signature
    [
      'id'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    begin
      user.notifications.find(data[:id]).delete
      {status: 'SUCCESS'}
    rescue
      {error: 'NOEXIST'}
    end
  end
end