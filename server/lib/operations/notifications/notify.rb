class Notify < Operation
  def signature
    [
      'username',
      'source',
      'message'
    ]
  end

  def authenticator
    'arbiter || wizard'
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    if u
      Notification.create(
        user: u,
        source: data[:source],
        message: data[:message]
      )
      { status: 'SUCCESS' }
    else
      {error: 'NOEXIST'}
    end
  end
end