class GrantCredits < Operation
  def signature
    [
      'username',
      'credits'
    ]
  end

  def authenticator
    'wizard || arbiter'
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    if u
      u.credits += data[:credits].to_i
      u.save
      { status: 'SUCCESS' }
    else
      {error: 'NOEXIST'}
    end
  end
end