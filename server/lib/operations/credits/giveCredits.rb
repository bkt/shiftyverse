class GiveCredits < Operation
  def signature
    [
      'username',
      'credits'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    credits = +data[:credits].to_i
    if u
      if user.credits >= credits
        user.credits -= credits
        user.save
        u.credits += credits
        u.save
        { status: 'SUCCESS' }
      else
        { error: 'NOCREDIT'}
      end
    else
      {error: 'NOEXIST'}
    end
  end
end