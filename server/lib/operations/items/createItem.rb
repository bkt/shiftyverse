class CreateItem < Operation
  def signature
    [
      'name',
      'description'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    if i = Item.create(name: data[:name], description: data[:description])
      i
    else
      { error: 'CRETFAIL' }
    end
  end
end