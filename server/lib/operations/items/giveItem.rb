class GiveItem < Operation
  def signature
    [
      'username',
      'item_id',
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    if u
      my_inv = user.inventory
      their_inv = u.inventory
      ii = my_inv.item_instances.where(id: data[:item_id]).first
      if ii
        nii = ii.dup
        their_inv.item_instances << nii
        ii.delete
        my_inv.save
        their_inv.save
        nii
      else
        { error: 'NOINV' }
      end
    else
      { error: 'NOEXIST' }
    end
  end
end