class DefineItemAction < Operation
  def signature
    [
      'item',
      'name',
      'script'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    if i = Item.where(name: data[:item]).first
      i.actions[data[:name]] = data[:script]
      i.save
      i
    else
      { error: 'NOEXIST'}
    end
  end
end