class InteractItem < Operation
  def signature
    [
      'item_id',
      'action',
      'arguments'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    i = user.inventory.item_instances.where(id: data[:item_id]).first
    if i
      if i.item.actions.keys.include?(data[:action])
        engine = ScriptEngine.new(i.item.actions[data[:action]], user, i)
        engine.run(data[:arguments])
        engine.result
      else
        return { error: 'NOEXIST' }
      end
      
    else
      return { error: 'NOEXIST' }
    end
  end
end