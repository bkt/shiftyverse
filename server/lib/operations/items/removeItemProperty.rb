class RemoveItemProperty < Operation
  def signature
    [
      'item',
      'property'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    if i = Item.where(name: data[:item]).first
      i.property_template.delete data[:property]
      i.save
      i
    else
      { error: 'NOEXIST'}
    end
  end
end