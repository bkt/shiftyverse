class GetItem < Operation
  def signature
    [
      'item'
    ]
  end

  def authenticator
    true
  end

  def run(user,data)
    if i = Item.where(name: data[:item]).first
      i
    else
      { error: 'NOEXIST' }
    end
  end
end