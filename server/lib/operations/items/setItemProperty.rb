class SetItemProperty < Operation
  def signature
    [
      'item_id',
      'property',
      'value'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    if i = user.inventory.item_instances.where(id: data[:item_id]).first
      binding.pry
      i.properties[data[:property]] = data[:value]
      i.save
      i
    else
      { error: 'NOEXIST' }
    end
  end
end