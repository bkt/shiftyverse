class GrantItem < Operation
  def signature
    [
      'username',
      'item',
    ]
  end

  def authenticator
    'wizard'
  end

  def run(user,data)
    u = User.where(username: data[:username]).first
    if u
      i = Item.where(name: data[:item]).first
      if i
        ii = ItemInstance.create(inventory: u.inventory, item: i)
        ii
      else
        {error: 'NOEXIST'}
      end
    else
      {error: 'NOEXIST'}
    end
  end
end