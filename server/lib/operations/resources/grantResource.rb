class GrantResource < Operation
  def signature
    [
      'username',
      'resource',
      'quantity'
    ]
  end

  def authenticator
    'wizard'
  end

  def run(user,data)
    u = User.where(username: data[:username]).first
    if u
      r = Resource.where(name: data[:resource]).first
      if r
        ri = u.inventory.resource_inventories.find_or_create_by(resource_id: r.id)
        ri.quantity += data[:quantity].to_i
        ri.save
        ri
      else
        {error: 'NOEXIST'}
      end
    else
      {error: 'NOEXIST'}
    end
  end
end