class Forage < Operation
  def signature
    []
  end

  def authenticator
    true
  end

  def run(user, data)
    lt = LootTable.first

    loot = [] 
    lt.table.each do |k,v|
      if r = Resource.find_by(name: k)
        v.times do 
          loot << r
        end
      else
      end
    end

    r = loot.sample

    ri = user.inventory.resource_inventories.find_or_create_by(resource_id: r.id)
    ri.quantity += 1
    ri.save
    ri
  end
end