class ListResources < Operation
  def signature
    []
  end

  def authenticator
    true
  end

  def run(user, data)
    { resources: Resource.all }
  end
end