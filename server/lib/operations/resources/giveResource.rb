class GiveResource < Operation
  def signature
    [
      'username',
      'resource',
      'quantity'
    ]
  end

  def authenticator
    true
  end

  def run(user,data)
    qty = data[:quantity].to_i
    u = User.where(username: data[:username]).first
    if u
      r = Resource.where(name: data[:resource]).first
      if r
        i = user.inventory.resource_inventories.where(resource: r).first
        iq = i&.quantity || 0
        if iq && iq >= qty
          i.quantity -= qty
          i.save
          ri = ResourceInventory.find_or_create_by(inventory: u.inventory, resource: r)
          ri.quantity += qty
          ri.save
          { status: 'SUCCESS' }
        else
          { error: 'NOINV' }
        end
      else
        return {error: 'NOEXIST'}
      end
    else
      {error: 'NOEXIST'}
    end
  end
end