class CreateResource < Operation
  def signature
    [
      'name',
      'description'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    if res = Resource.create(name: data[:name], description: data[:description])
      res
    else
      { error: 'CRETFAIL'}
    end
  end
end