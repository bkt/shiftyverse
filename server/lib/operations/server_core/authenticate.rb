class Authenticate < Operation
  include SecurePassword
  def signature
    [
      'username',
      'password'
    ]
  end

  def authenticator
    nil
  end

  def run(user, data)
    u = User.where(username: data[:username]).first
    return {error: 'AUTHFAIL'} unless u

    if checkPassword(data[:password], u.password_hash, u.password_salt)
      return {auth_token: { token: AuthToken.new({id: u.id }).to_token }, user_state: u.to_state}
    else
      return {error: 'AUTHFAIL'}
    end
  end
end