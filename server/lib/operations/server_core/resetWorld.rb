class ResetWorld < Operation
  def signature
    []
  end

  def authenticator
    '[wizard]'
  end

  def run(user, data)
    User.all.each(&:delete)
    {status: 'success'}
  end
end