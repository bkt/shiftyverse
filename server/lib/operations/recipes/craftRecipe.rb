class CraftRecipe < Operation
  def signature
    [
      'recipe_id'
    ]
  end

  def authenticator
    true
  end

  def run(user, data)
    begin
      r = Recipe.find(data[:recipe_id])
    rescue
      return { error: 'NOEXIST', target: 'recipe'}
    end

    rbuckets = {}
    r.resources.each do |rs|
      rbuckets[rs] ||= 0
      rbuckets[rs] += 1
    end

    ibuckets = {}
    r.items.each do |is|
      ibuckets[is] ||= 0
      ibuckets[is] += 1
    end

    rbuckets.each do |r,c|
      r = Resource.where(name: r).first
      if r
        iq = user.inventory.resource_inventories.where(resource: r).first&.quantity || 0
        return { error: 'NOINV' } unless iq >= c
      else
        return { error: 'BADREC'}
      end
    end

    ibuckets.each do |i,c|
      i = Item.where(name: i).first
      if i
        iq = user.inventory.item_instances.where(item: i).count
        return { error: 'NOINV' } unless iq >= c
      else
        return { error: 'BADREC'}
      end
    end

    r.products.map do |p|
      i = Item.where(name: p).first
      return { error: 'BADREC' } unless i
      i
    end

    rbuckets.each do |r,c|
      r = Resource.where(name: r).first
      inv = user.inventory.resource_inventories.where(resource: r).first
      inv.quantity -= c
      inv.save
    end

    ibuckets.each do |i,c|
      i = Item.where(name: i).first
      user.inventory.item_instances.where(item: i).first(c).each(&:delete)
    end

    rx = r.products.map do |p|
      i = Item.where(name: p).first
      ii = ItemInstance.create(inventory: user.inventory, item: i)
    end

    { products: rx }
  end
end