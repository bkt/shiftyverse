class CreateRecipe < Operation
  def signature
    [
      'resources',
      'items',
      'products'
    ]
  end

  def authenticator
    'wizard || architect'
  end

  def run(user, data)
    rs = []
    (data[:resources] || '').split(',').each do |r|
      if Resource.where(name: r).first
        rs << r
      else
        return { error: 'CRETFAIL' }
      end
    end

    is = []
    (data[:items] || '').split(',').each do |i|
      if Item.where(name: i).first
        is << i
      else
        return { error: 'CRETFAIL' }
      end
    end

    prs = []
    (data[:products] || '').split(',').each do |i|
      if Item.where(name: i).first
        prs << i
      else
        return { error: 'CRETFAIL' }
      end
    end


    if r =  Recipe.create(items: is, resources: rs, products: prs)
      r
    else
      return { error: 'CRETFAIL' }
    end
  end
end