class ListRecipes < Operation
  def signature
    []
  end

  def authenticator
    true
  end

  def run(user, data)
    { recipes: Recipe.all }
  end
end