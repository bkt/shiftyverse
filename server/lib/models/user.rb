class User
  include Mongoid::Document
  include Mongoid::Geospatial

  field :username, type: String
  field :password_hash, type: String
  field :password_salt, type: String
  field :location, type: Point, sphere: true, default: [0,0]
  field :last_seen, type: Time, default: nil
  field :tags, type: Array, default: []
  field :credits, type: Integer, default: 0

  validates :username, uniqueness: true

  embeds_many :notifications
  has_one :inventory, as: :inventoryable
  has_many :properties, as: :ownable

  before_create :grant_first_user
  after_create :create_inventory

  def to_state
    {
      username: username,
      tags: tags,
      location: location,
      credits: credits,
      notifications: notifications,
      resources: inventory.resources,
      items: inventory.items,
      properties: properties
    }
  end

  def public_state
    {
      username: username
    }
  end

  private

  def grant_first_user
    if User.all.count == 0
      self.tags = ['wizard', 'arbiter', 'architect']
    end
  end

  def create_inventory
    Inventory.create(inventoryable: self)
  end
end