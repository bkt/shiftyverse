class LootTable
  include Mongoid::Document

  field :name, type: String
  field :table, type: Hash
end