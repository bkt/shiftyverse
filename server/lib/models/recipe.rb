class Recipe
  include Mongoid::Document

  field :resources, type: Array, default: []
  field :items, type: Array, default: []
  field :products, type: Array, default: []
end