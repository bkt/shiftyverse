class ItemInstance
  include Mongoid::Document

  field :id, type: BSON::ObjectId
  field :properties, type: Hash, default: {}

  belongs_to :item
  embedded_in :inventory
end