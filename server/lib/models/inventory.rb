class Inventory
  include Mongoid::Document

  belongs_to :inventoryable, polymorphic: true

  embeds_many :resource_inventories
  embeds_many :item_instances

  def resources
    resource_inventories.reduce({}) { |a, v| a.merge({v.resource.name => v.quantity}) }
  end

  def items
    item_instances.map { |ii|  ii.item.as_json.merge(ii.as_json).tap { |ii| ii.delete '_id'; ii.delete 'property_template'; ii['actions'] = ii['actions'].keys }}
  end
end