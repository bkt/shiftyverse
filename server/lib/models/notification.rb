class Notification
  include Mongoid::Document
  include Mongoid::Timestamps::Created

  field :source, type: String
  field :message, type: String

  embedded_in :user
end