class Property
  include Mongoid::Document
  include Mongoid::Geospatial

  field :name, type: String
  field :location, type: Point, sphere: true, default: [0,0]
  field :actions, type: Hash, default: {}
  field :security, type: Hash, default: {}

  belongs_to :ownable, polymorphic: true
  has_one :inventory, as: :inventoryable

  after_create :create_inventory

  private

  def create_inventory
    Inventory.create(inventoryable: self)
  end
end