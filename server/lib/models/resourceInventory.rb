class ResourceInventory
  include Mongoid::Document

  field :quantity, type: Integer, default: 0

  belongs_to :resource
  embedded_in :inventory
end