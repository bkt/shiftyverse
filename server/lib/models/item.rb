class Item
  include Mongoid::Document

  field :name, type: String
  field :description, type: String
  field :property_template, type: Array, default: []
  field :actions, type: Hash, default: {}
  field :metadata, type: Hash, default: {}

  validates :name, uniqueness: true
end