require 'faye'
require File.expand_path('../server', __FILE__)

use Faye::RackAdapter, :mount => '/faye', :timeout => 25

run ShiftyverseServer